from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.request
import re

html = urllib.request.urlopen('https://www.youtube.com/watch?v=pShL9DCSIUw').read()

def text_from_html(body):
    text=[]
    soup = BeautifulSoup(body, 'html.parser')
    texts = soup.findAll(text=True)
    visible_texts = filter(tag_visible, texts)  
    
    for word in visible_texts:
        text.extend(word.split())

    return set(text)


def tag_visible(element):
    if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
        return False
    if isinstance(element, Comment):
        return False

    return True


def interpunction_remover(wordList):
    list = wordList
    list2 = []
    line=0
    for e in list:
        #line+=1
        e = e.replace('.', '').replace(',', ' ').replace('\n', ' ').replace('\r', ' ').replace(':', ' ').replace('"', ' ').replace('?', ' ')
        list2.append(e)
        #print(e + str(line))
    return list2


wordList = interpunction_remover(text_from_html(html))



print(wordList)

print("set length: " + str(len(wordList)))

f=open("webpage.txt", "w+")
f.write(str(wordList))
f.close()